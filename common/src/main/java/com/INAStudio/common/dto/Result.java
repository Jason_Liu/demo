package com.INAStudio.common.dto;

import java.io.Serializable;

public class Result<T> implements Serializable {
    private boolean state;
    private T data;
    private String message;

    public Result(boolean state, String message){
        this.state = state;
        this.message = message;
    }

    public Result(boolean state, T data) {
        this.state = state;
        this.data = data;
    }

    public Result(boolean state, T data, String message) {
        this.state = state;
        this.data = data;
        this.message = message;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Result{" +
                "state=" + state +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }
}
