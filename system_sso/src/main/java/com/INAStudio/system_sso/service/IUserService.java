package com.INAStudio.system_sso.service;

import com.INAStudio.common.dto.Result;
import com.INAStudio.system_sso.parent_sso.entity.User;

public interface IUserService {
    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    Result<User> login(String username, String password);

    /**
     * 验证token
     * @param token
     * @return
     */
    Result<User> auth(String token);

    /**
     * 通过用户名获取用户信息
     * @param username
     * @return
     */
    Result<User> getUserByUsername(String username);

    /**
     * 通过token获取用户信息
     * @param token
     * @return
     */
    Result<User> getUserByToken(String token);

    /**
     * 注册新用户
     * @param username
     * @param password
     * @param email
     * @return
     */
    Result<User> register(String username, String password, String email);

    /**
     * 通过用户名修改用户信息
     * @param username
     * @param password
     * @param email
     * @return
     */
    Result<User> changeUserInfoByUsername(String username, String password, String email);

    /**
     * 通过token修改用户信息
     * @param token
     * @param password
     * @param email
     * @return
     */
    Result<User> changeUserInfoByToken(String token, String password, String email);
}
