package com.INAStudio.system_sso.service_sso.service.impl;

import com.INAStudio.common.dto.Result;
import com.INAStudio.system_sso.parent_sso.entity.User;
import com.INAStudio.system_sso.service.IUserService;

public class UserService implements IUserService {
    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    @Override
    public Result<User> login(String username, String password) {
        //Detailed code logic
        return null;
    }

    /**
     * 验证token
     *
     * @param token
     * @return
     */
    @Override
    public Result<User> auth(String token) {
        //Detailed code logic
        return null;
    }

    /**
     * 通过用户名获取用户信息
     *
     * @param username
     * @return
     */
    @Override
    public Result<User> getUserByUsername(String username) {
        //Detailed code logic
        return null;
    }

    /**
     * 通过token获取用户信息
     *
     * @param token
     * @return
     */
    @Override
    public Result<User> getUserByToken(String token) {
        //Detailed code logic
        return null;
    }

    /**
     * 注册新用户
     *
     * @param username
     * @param password
     * @param email
     * @return
     */
    @Override
    public Result<User> register(String username, String password, String email) {
        //Detailed code logic
        return null;
    }

    /**
     * 通过用户名修改用户信息
     *
     * @param username
     * @param password
     * @param email
     * @return
     */
    @Override
    public Result<User> changeUserInfoByUsername(String username, String password, String email) {
        //Detailed code logic
        return null;
    }

    /**
     * 通过token修改用户信息
     *
     * @param token
     * @param password
     * @param email
     * @return
     */
    @Override
    public Result<User> changeUserInfoByToken(String token, String password, String email) {
        //Detailed code logic
        return null;
    }
}
