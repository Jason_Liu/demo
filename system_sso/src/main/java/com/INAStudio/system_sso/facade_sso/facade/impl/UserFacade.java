package com.INAStudio.system_sso.facade_sso.facade.impl;

import com.INAStudio.system_sso.facade.IUserFacade;
import com.INAStudio.system_sso.parent_sso.entity.User;

public class UserFacade implements IUserFacade {
    /**
     * 检查用户是否存在
     *
     * @param username
     * @return
     */
    @Override
    public boolean isUserExsits(String username) {
        //Detailed code logic
        return false;
    }

    /**
     * 检查密码是否正确
     *
     * @param username
     * @param password
     * @return
     */
    @Override
    public boolean isPasswordCorrect(String username, String password) {
        //Detailed code logic
        return false;
    }

    /**
     * 通过用户名获取用户信息
     *
     * @param username
     * @return
     */
    @Override
    public User getUserByUsername(String username) {
        //Detailed code logic
        return null;
    }

    /**
     * 通过token获取用户信息
     *
     * @param token
     * @return
     */
    @Override
    public User getUserByToken(String token) {
        //Detailed code logic
        return null;
    }

    /**
     * 添加新用户
     *
     * @param username
     * @param password
     * @param email
     * @return
     */
    @Override
    public User insertNewUser(String username, String password, String email) {
        //Detailed code logic
        return null;
    }

    /**
     * 修改密码
     *
     * @param password
     * @param username
     * @return
     */
    @Override
    public User changePassword(String password, String username) {
        //Detailed code logic
        return null;
    }
}
