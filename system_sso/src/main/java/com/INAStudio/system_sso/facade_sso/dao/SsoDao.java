package com.INAStudio.system_sso.facade_sso.dao;

import com.INAStudio.system_sso.parent_sso.entity.User;
import org.apache.ibatis.annotations.Param;

public interface SsoDao {
    /**
     * 通过id获取用户信息
     * @param id
     * @return
     */
    User getUserById(@Param("id") Integer id);

    /**
     * 通过用户名获取用户信息
     * @param username
     * @return
     */
    User getUserByUsername(@Param("username") String username);

    /**
     * 增加用户
     * @param username
     * @param password
     * @param email
     * @param modifiedTime
     * @return
     */
    Integer insertNewUser(@Param("username") String username, @Param("password") String password,
                       @Param("email") String email, @Param("modifiedTime") String modifiedTime);

    Integer updateUser(@Param("username") String username, @Param("password") String password,
                       @Param("email") String email, @Param("modifiedTime") Integer modifiedTime);
}
