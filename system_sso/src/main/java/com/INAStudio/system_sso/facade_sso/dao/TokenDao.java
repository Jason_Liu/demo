package com.INAStudio.system_sso.facade_sso.dao;

import com.INAStudio.system_sso.parent_sso.entity.Token;
import org.apache.ibatis.annotations.Param;

public interface TokenDao {
    /**
     * 向token记录表中添加token记录
     * @param userId
     * @param token
     * @return
     */
    Integer insertToken(@Param("userId") Integer userId, @Param("token") String token,@Param("modifiedTime") String modifiedTime);

    /**
     * 通过用户id获取Token记录
     * @param userId
     * @return
     */
    Token getTokenByUser(@Param("userId") Integer userId);

    /**
     * 更新token
     * @param userId
     * @param token
     * @return
     */
    Integer updateToken(@Param("userId") Integer userId, @Param("token") String token);
}
