package com.INAStudio.system_sso.parent_sso.entity;

public class Token {
    private Integer id;
    private Integer userId;
    private String token;
    private Integer modifiedTime;

    public Token(Integer id, Integer userId, String token, Integer modifiedTime) {
        this.userId = userId;
        this.id = id;
        this.token = token;
        this.modifiedTime = modifiedTime;
    }

    public Integer getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Integer modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Token{" +
                "userId=" + userId +
                ", id=" + id +
                ", token='" + token + '\'' +
                ", modifiedTime='" + modifiedTime + '\'' +
                '}';
    }
}
