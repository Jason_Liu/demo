package com.INAStudio.system_sso.parent_sso.entity;


public class User {
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Integer modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    private Integer id;
    private String username;
    private String password;
    private String email;
    private Integer modifiedTime;

    public User(Integer id, String username, String password, String email, Integer modifiedTime) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.modifiedTime = modifiedTime;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", modifiedTime='" + modifiedTime + '\'' +
                '}';
    }
}
