package com.INAStudio.system_sso.facade;

import com.INAStudio.system_sso.parent_sso.entity.User;

public interface IUserFacade {
    /**
     * 检查用户是否存在
     * @param username
     * @return
     */
    boolean isUserExsits(String username);

    /**
     * 检查密码是否正确
     * @param username
     * @param password
     * @return
     */
    boolean isPasswordCorrect(String username, String password);

    /**
     * 通过用户名获取用户信息
     * @param username
     * @return
     */
    User getUserByUsername(String username);

    /**
     * 通过token获取用户信息
     * @param token
     * @return
     */
    User getUserByToken(String token);

    /**
     * 添加新用户
     * @param username
     * @param password
     * @param email
     * @return
     */
    User insertNewUser(String username, String password, String email);

    /**
     * 修改密码
     * @param password
     * @param username
     * @return
     */
    User changePassword(String password, String username);
}
