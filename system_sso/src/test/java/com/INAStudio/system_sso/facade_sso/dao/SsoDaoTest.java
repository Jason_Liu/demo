package com.INAStudio.system_sso.facade_sso.dao;

import com.INAStudio.system_sso.parent_sso.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:system_sso/spring/*.xml")
public class SsoDaoTest {

    @Autowired
    SsoDao ssoDao;

    @Test
    public void insertNewUser() {
        System.out.println(ssoDao.insertNewUser("Jsn_Liu","admin","Jsn@test.com","123456"));
    }

    @Test
    public void getUserById() {
        System.out.println(ssoDao.getUserById(1));
    }

    @Test
    public void getUserByUsername() {
        System.out.println(ssoDao.getUserByUsername("Jsn_Liu"));
    }

    @Test
    public void updateUser() {
        System.out.println(ssoDao.updateUser("Jsn_Liu","ADMIN", "admin@admin.com", 123591));
    }
}