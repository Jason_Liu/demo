package com.INAStudio.system_sso.facade_sso.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:system_sso/spring/*.xml")
public class TokenDaoTest {

    @Autowired
    TokenDao tokenDao;
    @Test
    public void insertToken() {
        tokenDao.insertToken(1,"1s23afe5fhu3k85","123465897");
    }

    @Test
    public void getTokenByUser() {
        tokenDao.getTokenByUser(1);
    }

    @Test
    public void updateToken() {
        tokenDao.updateToken(1,"1s23afe5fhu3k85");
    }
}